public class Part {
    private String corporatePartNumber;
    private String type;
    private String description;

    private int quantity;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCorporatePartNumber() {
        return corporatePartNumber;
    }

    public void setCorporatePartNumber(String corporatePartNumber) {
        this.corporatePartNumber = corporatePartNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
