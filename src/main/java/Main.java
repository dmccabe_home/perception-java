import org.codehaus.jackson.map.ObjectMapper;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main extends HttpServlet {

    private static List<Part> parts;
    private static List<Bom> boms;

    private void getParts(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        resp.getWriter().write(objectMapper.writeValueAsString(parts));
    }

    private void getBoms(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        resp.getWriter().write(objectMapper.writeValueAsString(boms));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        addHeaders(resp);

        if (req.getRequestURI().endsWith("/parts")) {
            getParts(req, resp);
            return;
        }


        if(req.getRequestURI().endsWith("/boms")) {
            getBoms(req, resp);
            return;
        }

        showHome(req, resp);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) {
        addHeaders(resp);
    }

    private void addHeaders(HttpServletResponse resp) {
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Type");
    }

    // Insert
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if(req.getRequestURI().matches("/boms")) {
            addHeaders(resp);
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = req.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }
            String json = buffer.toString();

            if(json != null && !json.equals("")) {
                ObjectMapper objectMapper = new ObjectMapper();
                Bom bom = objectMapper.readValue(json, Bom.class);
                bom.setBomNumber(UUID.randomUUID().toString());
                boms.add(bom);
            }
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        addHeaders(resp);

        if(req.getRequestURI().contains("/boms/")) {
            String uri = req.getRequestURI();
            String[] splitUri = uri.split("/");
            String bomNumber = splitUri[splitUri.length-1];
            if(!bomNumber.equals("")) {
                for (Bom bom : boms) {
                    if (bom.getBomNumber().equals(bomNumber)) {
                        boms.remove(bom);
                        break;
                    }
                }
            }
        }
    }

    // Update
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        if(req.getRequestURI().endsWith("/boms")) {
            addHeaders(resp);
            StringBuilder buffer = new StringBuilder();
            BufferedReader reader = req.getReader();
            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            String json = buffer.toString();

            if (!json.equals("")) {
                ObjectMapper objectMapper = new ObjectMapper();
                Bom bom = objectMapper.readValue(json, Bom.class);

                for (Bom b : boms) {
                    if (b.getBomNumber().equals(bom.getBomNumber())) {
                        boms.remove(b);
                        boms.add(bom);
                        break;
                    }
                }
            }
        }
    }

    private void showHome(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.sendRedirect("index.html");
    }

    public static void main(String[] args) throws Exception {
        loadTestData();


        // Create sample bom for testing
        boms = new ArrayList<Bom>();
        List<BomPart> bomParts = new ArrayList<BomPart>();
        BomPart bomPart = new BomPart();
        bomPart.setPartNumber(parts.get(0).getCorporatePartNumber());
        bomPart.setQuantity(4);
        bomParts.add(bomPart);
        Bom bom = new Bom();
        bom.setDescription("Test Description");
        bom.setBomNumber("23982SDFS");
        bom.setParts(bomParts);
        boms.add(bom);

        String port = System.getenv("PORT");
        if(port == null) {
            port = "8080";
        }

        Server server = new Server(Integer.valueOf(port));
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        context.addServlet(new ServletHolder(new Main()),"/*");
        server.start();
        server.join();
    }

    private static void loadTestData() {
        String capacitorCSV = "Capacitors.csv";
        String diodesCSV = "Diodes.csv";
        String resistorsCSV = "Resistors.csv";
        String line = "";

        BufferedReader bufferedReader = null;

        // Parse capacitors
        try {
            parts = new ArrayList<Part>();
            Hashtable<String, String> files = new Hashtable<String, String>();
            files.put("Capacitor", "Capacitors.csv");
            files.put("Diode", "Diodes.csv");
            files.put("Resistor", "Resistors.csv");

            for (Map.Entry file : files.entrySet()) {
                bufferedReader = new BufferedReader(new FileReader(file.getValue().toString()));

                // Skip first line that has column names
                bufferedReader.readLine();

                while((line = bufferedReader.readLine()) != null) {
                    String[] columns = line.split(",");
                    Part capacitorPart = new Part();
                    capacitorPart.setCorporatePartNumber(columns[2]);
                    capacitorPart.setDescription(columns[5]);
                    capacitorPart.setType(file.getKey().toString());
                    parts.add(capacitorPart);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
