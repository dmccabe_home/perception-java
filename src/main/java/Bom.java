import java.util.List;
import java.util.Map;

public class Bom {
    private String bomNumber;
    private String description;
    private List<BomPart> parts;


    public String getBomNumber() {
        return bomNumber;
    }

    public void setBomNumber(String bomNumber) {
        this.bomNumber = bomNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BomPart> getParts() {
        return parts;
    }

    public void setParts(List<BomPart> p) {
        this.parts = p;
    }
}
