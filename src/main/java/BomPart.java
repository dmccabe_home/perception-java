public class BomPart {
    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int numberOfParts) {
        this.quantity = numberOfParts;
    }

    private String partNumber;
    private int quantity;
}
